# StarLoco Continued: Lobby-Server : Configuration

### File & Location
The configuration file should be called `config.txt`.

If you are using the .jar executable, this config file is to be found under the working directory.

If you are using the docker service, the file is to be mounted under: `/var/lib/dofus/config.txt` (ro)

Here is the example of an exhaustive useless config file : [examples/config.txt](../examples/config.txt).
It is exhaustive because every option is present, and useless because each (non-mandatory) option is affected the default associated value.

### Content (`VAR_NAME` [= <default_value>] : <info / comment>)
 * `LOBBY_LISTENING_PORT` = 443 : The port clients will use to join the server. If you want your server to be available locally, a firewall rule must be added on the host. And if you want your server to be available remotely, the router must redirect traffic to the host on this port.

 * `EXCHANGE_LISTENING_HOST` = 0.0.0.0 : The IP / hostname game-servers will use to join the lobby-server using the exchange protocol.
 * `EXCHANGE_LISTENING_PORT` = 5555 : The port game-servers will use to join the lobby-server using the exchange protocol.


 * `DB_LOBBY_HOST` = 127.0.0.1 : The IP / hostname on which to reach the lobby / characters database. You don't need to specify it (=default) if the lobby-server is running on the same host as the aforementioned database.
 * `DB_LOBBY_PORT` = 3306 : The port on which to reach the lobby /characters database. You don't need to specify it if you used the default listening port for your Maria/MySQL database.
 * `DB_LOBBY_USER` = root : The user with which to reach the game database. You don't need to specify it if you want to use the root user (not recommended).
 * `DB_LOBBY_PASS`: The password for the aforementioned user (mandatory / no default).
 * `DB_LOBBY_NAME` = characters : The name of the schema containing game data on the lobby / characters database. You don't need to specify it if you called the schema "characters".


 * `CLIENT_VERSION` = 1.29.1 : The client versions the server is expected to serve.

