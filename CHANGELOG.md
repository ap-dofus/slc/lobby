Release-0.1:
  - Sources encoding change from `windows-1252` to `utf-8`
  - Refactoring of the config loading strategy to be more similar to the one of the game-server.

Release-0.0:
  - Fork & segmentation from https://gitlab.lemnoslife.com/Benjamin_Loison/dofus-retro-server