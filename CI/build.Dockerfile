# Lightweight linux image.
ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION}

# Install JRE & Maven.
ARG JAVA_VERSION=8
RUN apk --no-cache --progress update
RUN apk --no-cache --progress upgrade
RUN apk --no-cache --progress add openjdk${JAVA_VERSION}-jre maven
