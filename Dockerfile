# Lightweight linux image.
ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION}

# Install JRE.
ARG JAVA_VERSION=8
RUN apk --no-cache --progress update
RUN apk --no-cache --progress upgrade
RUN apk --no-cache --progress add openjdk${JAVA_VERSION}-jre
RUN rm -rf /var/cache/apk/*

# Server executable.
RUN mkdir -p /opt/dofus/
ARG EXEC_PATH="target/lobby-*.jar"
COPY ${EXEC_PATH} /opt/dofus/lobby-server.jar

# Where the config should be located.
WORKDIR /var/lib/dofus

# Additional java arguments as env variable.
ENV JAVA_OPTS="-Xms256m -Xmx256m"
ENTRYPOINT java -jar ${JAVA_OPTS} "/opt/dofus/lobby-server.jar"

