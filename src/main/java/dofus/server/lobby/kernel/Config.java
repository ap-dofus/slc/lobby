package dofus.server.lobby.kernel;

import dofus.server.lobby.exchange.ExchangeServer;
import dofus.server.lobby.login.LoginServer;

import java.io.*;
import java.util.*;

public class Config {
    private static Config singleton = null;


    public class Connectable{
        public final String host;
        public final int port;

        public Connectable(String host, int port) {
            this.host = host;
            this.port = port;
        }
    }
    public class Authable extends Connectable{
        public final String username, password;

        public Authable(String host, int port, String username, String password) {
            super(host, port);
            this.username = username;
            this.password = password;
        }
    }
    public class DB extends Authable{
        public final String name;

        public DB(String host, int port, String username, String password, String name) {
            super(host, port, username, password);
            this.name = name;
        }
    }


    private class ConfigParam {
        public Optional<String> value;

        public ConfigParam(String defaultValue) {
            this.value = Optional.of(defaultValue);
        }
        public ConfigParam() {
            this.value = Optional.empty();
        }
    }
    private class ParamParser extends HashMap<String, ConfigParam> {
        public ParamParser() {
            super();
            this.put("LOBBY_LISTENING_PORT", new ConfigParam("443"));
            this.put("EXCHANGE_LISTENING_HOST", new ConfigParam("0.0.0.0"));
            this.put("EXCHANGE_LISTENING_PORT", new ConfigParam("666"));
            this.put("DB_LOBBY_HOST", new ConfigParam("127.0.0.1"));
            this.put("DB_LOBBY_PORT", new ConfigParam("3306"));
            this.put("DB_LOBBY_USER", new ConfigParam("root"));
            this.put("DB_LOBBY_PASS", new ConfigParam());
            this.put("DB_LOBBY_NAME", new ConfigParam("accounts"));
            this.put("CLIENT_VERSION", new ConfigParam("1.29.1"));
        }

        public void parse(BufferedReader reader) throws IOException {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.split("=").length != 2)
                    continue;

                String[] splitted = line.split("=");

                get(splitted[0].trim().replace(" ", "").toUpperCase()).value
                        = Optional.of(splitted[1].trim());
            }
        }

        public Integer getLobbyListeningPort(){
            return Integer.parseInt(get("LOBBY_LISTENING_PORT").value.get());
        }
        public Connectable getExchange(){
            return new Connectable(get("EXCHANGE_LISTENING_HOST").value.get(),
                    Integer.parseInt(get("EXCHANGE_LISTENING_PORT").value.get()));
        }

        private DB getDb(String prefix){
            return new DB(get("DB_" + prefix + "_HOST").value.get(),
                    Integer.parseInt(get("DB_" + prefix + "_PORT").value.get()),
                    get("DB_" + prefix + "_USER").value.get(),
                    get("DB_" + prefix + "_PASS").value.orElse(""),
                    get("DB_" + prefix + "_NAME").value.orElse(""));
        }
        public DB getLobbyDB(){
            return getDb("LOBBY");
        }

        public String getClientVersion(){
            return get("CLIENT_VERSION").value.get();
        }
    }

    // TODO: Nasty, nasty, nasty, can we have these 3 members somewhere else ? (Maybe in main ?)
    public static boolean isRunning;
    public static LoginServer loginServer;
    public static ExchangeServer exchangeServer;

    public static final long startTime = System.currentTimeMillis();

    public final int listeningPort;
    public final Connectable exchangeDefinition;
    public final DB lobbyDB;
    public final String clientVersion;

    public Config() {
        ParamParser params = new ParamParser();
        try{
            FileReader file = new FileReader("config.txt");
            try {
                params.parse(new BufferedReader(new FileReader("config.txt")));
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {}

        listeningPort = params.getLobbyListeningPort();
        exchangeDefinition = params.getExchange();
        lobbyDB = params.getLobbyDB();
        clientVersion = params.getClientVersion();
    }

    public static Config getInstance() {
        if (singleton == null)
            singleton = new Config();
        return singleton;
    }
}
