package dofus.server.lobby.kernel;

public enum EmulatorInfo {
    RELEASE(0.1), SOFT_NAME("StarLoco-Continued Lobby v" + RELEASE.value), HARD_NAME(
            SOFT_NAME + " pour Dofus " + Config.getInstance().clientVersion);

    private String string;
    private double value;

    private EmulatorInfo(String s) {
        this.string = s;
    }

    private EmulatorInfo(double d) {
        this.value = d;
    }

    public static String uptime() {
        long uptime = System.currentTimeMillis() - Config.startTime;
        int jour = (int) (uptime / (1000 * 3600 * 24));
        uptime %= (1000 * 3600 * 24);
        int hour = (int) (uptime / (1000 * 3600));
        uptime %= (1000 * 3600);
        int min = (int) (uptime / (1000 * 60));
        uptime %= (1000 * 60);
        int sec = (int) (uptime / (1000));

        return jour + "j " + hour + "h " + min + "m " + sec + "s";
    }

    @Override
    public String toString() {
        return this.string;
    }
}
