package dofus.server.lobby.database;

interface DAO<T> {

    T load(Object obj);

    boolean update(T obj);
}