package dofus.server.lobby.database;

import ch.qos.logback.classic.Logger;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import dofus.server.lobby.database.data.AccountData;
import dofus.server.lobby.database.data.PlayerData;
import dofus.server.lobby.database.data.ServerData;
import dofus.server.lobby.database.data.WorldEntityData;
import dofus.server.lobby.kernel.Config;
import dofus.server.lobby.kernel.Main;
import org.slf4j.LoggerFactory;

import java.sql.Connection;

public class Database {
    private static final Logger logger = (Logger) LoggerFactory.getLogger(Database.class);
    //connection
    private HikariDataSource dataSource;
    //data
    private AccountData accountData;
    private PlayerData playerData;
    private ServerData serverData;
    private WorldEntityData worldEntityData;

    void initializeData() {
        this.accountData = new AccountData(dataSource);
        this.playerData = new PlayerData(dataSource);
        this.serverData = new ServerData(dataSource);
        this.worldEntityData = new WorldEntityData(dataSource);
    }

    public void initializeConnection() {
        logger.trace("Reading database config");
        HikariConfig config = new HikariConfig();

        config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        //config.setDataSourceClassName("org.mariadb.jdbc.MySQLDataSource");
        config.addDataSourceProperty("serverName", Config.getInstance().lobbyDB.host);
        config.addDataSourceProperty("port", Config.getInstance().lobbyDB.port);
        config.addDataSourceProperty("databaseName", Config.getInstance().lobbyDB.name);
        config.addDataSourceProperty("user", Config.getInstance().lobbyDB.username);
        config.addDataSourceProperty("password", Config.getInstance().lobbyDB.password);

        dataSource = new HikariDataSource(config);
        if (!testConnection(dataSource)) {
            logger.error("Please check your username and password and database connection");
            Main.exit();
            return;
        }
        logger.info("Database connection established");
        this.initializeData();
    }

    public AccountData getAccountData() {
        return accountData;
    }

    public PlayerData getPlayerData() {
        return playerData;
    }

    public ServerData getServerData() {
        return serverData;
    }

    public WorldEntityData getWorldEntityData() {
        return worldEntityData;
    }


    private boolean testConnection(HikariDataSource dataSource) {
        try {
            Connection connection = dataSource.getConnection();
            connection.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
