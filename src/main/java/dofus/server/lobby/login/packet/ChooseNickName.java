package dofus.server.lobby.login.packet;

import dofus.server.lobby.kernel.Main;
import dofus.server.lobby.login.LoginClient;
import dofus.server.lobby.object.Account;

class ChooseNickName {

    public static void verify(LoginClient client, String nickname) {
        Account account = client.getAccount();

        if (!account.getPseudo().isEmpty()) {
            client.kick();
            return;
        }

        if (nickname.toLowerCase().equals(account.getName().toLowerCase())) {
            client.send("AlEr");
            return;
        }

        String s[] = {"admin", "modo", " ", "&", "é", "\"", "'",
                "(", "-", "è", "_", "ç", "à", ")", "=", "~", "#",
                "{", "[", "|", "`", "^", "@", "]", "}", "°", "+",
                "^", "$", "ù", "*", ",", ";", ":", "!", "<", ">",
                "¨", "£", "%", "µ", "?", ".", "/", "§", "\n", "`"};

        for (String value : s) {
            if (nickname.contains(value)) {
                client.send("AlEs");
                break;
            }
        }

        if (Main.database.getAccountData().exist(nickname) != null) {
            client.send("AlEs");
            return;
        }

        client.getAccount().setPseudo(nickname);
        client.setStatus(LoginClient.Status.SERVER);
        client.getAccount().setState(0);
        AccountQueue.verify(client);
    }
}
