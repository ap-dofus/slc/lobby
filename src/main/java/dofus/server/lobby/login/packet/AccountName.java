package dofus.server.lobby.login.packet;

import dofus.server.lobby.kernel.Config;
import dofus.server.lobby.kernel.Main;
import dofus.server.lobby.login.LoginClient;

class AccountName {

    public static void verify(LoginClient client, String name) {
        try {
            client.setAccount(Main.database.getAccountData().load(name.toLowerCase()));
            client.getAccount().setClient(client);
        } catch (Exception e) {
            client.send("AlEf");
            client.kick();
            return;
        }

        if (client.getAccount() == null) {
            client.send("AlEf");
            client.kick();
            return;
        }


        if (Config.loginServer.clients.containsKey(name)) // S'il est déjà en connexion, on le kick : pas bon, il faut vérifier le mdp avant
            Config.loginServer.clients.get(name).kick();
        Config.loginServer.clients.put(name, client);
        client.setStatus(LoginClient.Status.WAIT_PASSWORD);
    }
}
