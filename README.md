# StarLoco Continued: Lobby-Server
This project is a continuation of the support for the StarLoco lobby(/login)-server emulator.

It was initially forked and segmented from: https://gitlab.lemnoslife.com/Benjamin_Loison/dofus-retro-server

It is available either as a maven (.jar) executable package, or as docker service.

## Manuals:
* [config](man/CONFIG.md)
